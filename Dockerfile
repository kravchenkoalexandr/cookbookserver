FROM openjdk:8-jre-alpine

ENV APPLICATION_USER ktor
RUN adduser -D -g '' $APPLICATION_USER

RUN mkdir /web-app
RUN chown -R $APPLICATION_USER /web-app

USER $APPLICATION_USER

COPY /build/libs/cookbook.jar /web-app/cookbook.jar
WORKDIR /web-app

CMD ["java", "-server", "-jar", "cookbook.jar"]