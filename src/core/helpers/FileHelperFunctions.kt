package core.helpers

import java.io.File
import java.io.InputStream
import java.io.OutputStream
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.coroutines.yield

suspend fun saveFile(path: String, name: String, fileSource: InputStream) {
    val file = File(path + File.separator + name)

    if (file.exists()) {
        throw FileAlreadyExistsException(file)
    }
    withContext(Dispatchers.IO) {
        file.createNewFile()
    }
    fileSource.use { input ->
        file.outputStream().buffered().use {
            output -> input.copyToSuspend(output)
        }
    }
}

fun File.deleteIfExists(): Boolean {
    return if (exists()) delete() else true
}

suspend fun InputStream.copyToSuspend(
    out: OutputStream,
    bufferSize: Int = DEFAULT_BUFFER_SIZE,
    yieldSize: Int = 4 * 1024 * 1024,
    dispatcher: CoroutineDispatcher = Dispatchers.IO
): Long {
    return withContext(dispatcher) {
        val buffer = ByteArray(bufferSize)
        var bytesCopied = 0L
        var bytesAfterYield = 0L
        while (true) {
            val bytes = read(buffer).takeIf { it >= 0 } ?: break
            out.write(buffer, 0, bytes)
            if (bytesAfterYield >= yieldSize) {
                yield()
                bytesAfterYield %= yieldSize
            }
            bytesCopied += bytes
            bytesAfterYield += bytes
        }
        return@withContext bytesCopied
    }
}
