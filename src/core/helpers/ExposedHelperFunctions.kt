package core.helpers

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.jetbrains.exposed.exceptions.ExposedSQLException
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.transactions.transaction

typealias ExposedSQLExceptionsMapper = (ExposedSQLException) -> java.lang.Exception

suspend fun <T> executeTransaction(
    db: Database? = null,
    mapper: ExposedSQLExceptionsMapper? = null,
    statement: Transaction.() -> T
): T {
    return withContext(Dispatchers.IO) {
        try {
            transaction(db) { statement() }
        } catch (exception: ExposedSQLException) {
            throw mapper?.invoke(exception) ?: exception
        }
    }
}
