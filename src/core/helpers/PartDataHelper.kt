package core.helpers

import io.ktor.http.ContentType
import io.ktor.http.content.PartData

data class PartDataImages(
    val name: String,
    val extension: String,
    val partData: PartData.FileItem
)

fun Iterable<PartData>.getFormDataValue(name: String): String? {
    return filterIsInstance<PartData.FormItem>().firstOrNull { it.name == name }?.value
}

class PartDataImagesMap(parts: List<PartData>) :
    HashMap<String, PartDataImages>(getPartDataImages(parts).associateBy { it.name }) {

    companion object {
        private fun getPartDataImages(parts: List<PartData>): List<PartDataImages> {
            return parts.filterIsInstance<PartData.FileItem>().mapNotNull { partFile ->
                partFile.contentType?.let {
                    val isAvailableFormat = it == ContentType.Image.PNG || it == ContentType.Image.JPEG
                    val originalFileName = partFile.originalFileName
                    if (isAvailableFormat && !originalFileName.isNullOrBlank()) {
                        return@let PartDataImages(originalFileName, '.' + it.contentSubtype, partFile)
                    } else {
                        return@let null
                    }
                }
            }
        }
    }
}
