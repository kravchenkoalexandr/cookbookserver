package core.repositories.recipes

import core.helpers.ExposedSQLExceptionsMapper

object RecipesRepositoryMappers {
    val searchRecipe: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> RecipeUnknownException(exception.message)
        }
    }

    val createStep: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            "23505" -> StepAlreadyExistsException(exception.message)
            else -> StepUnknownException(exception.message)
        }
    }

    val searchStep: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> StepUnknownException(exception.message)
        }
    }
}
