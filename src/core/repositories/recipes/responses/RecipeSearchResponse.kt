package core.repositories.recipes.responses

import core.db.models.Recipes
import core.repositories.nationalCuisines.NationalCuisinesUnableMapRawNationalCuisinesException
import org.jetbrains.exposed.sql.ResultRow

data class RecipeSearchResponse(
    val id: Int,
    val title: String,
    val description: String?,
    val authorId: Int,
    val stepsCount: Int,
    val likesCount: Int,
    val imageId: Int?,
    val chefId: Int?,
    val nationalCuisineId: Int?
)

fun ResultRow.toRecipe(): RecipeSearchResponse? {
    return if (
        hasValue(Recipes.id) &&
        hasValue(Recipes.title) &&
        hasValue(Recipes.description) &&
        hasValue(Recipes.author_id) &&
        hasValue(Recipes.steps_count) &&
        hasValue(Recipes.likes_count) &&
        hasValue(Recipes.image_id) &&
        hasValue(Recipes.chef_id) &&
        hasValue(Recipes.national_cuisines_id)
    ) {
        RecipeSearchResponse(
            get(Recipes.id),
            get(Recipes.title),
            get(Recipes.description),
            get(Recipes.author_id),
            get(Recipes.steps_count),
            get(Recipes.likes_count),
            get(Recipes.image_id),
            get(Recipes.chef_id),
            get(Recipes.national_cuisines_id)
        )
    } else { null }
}
