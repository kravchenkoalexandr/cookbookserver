package core.repositories.recipes.responses

import core.db.models.RecipeSteps
import org.jetbrains.exposed.sql.ResultRow

data class StepSearchResponse(
    val id: Int,
    val title: String,
    val body: String,
    val recipeId: Int,
    val number: Int,
    val imageId: Int?
)

fun ResultRow.toStep(): StepSearchResponse? {
    return if (
        hasValue(RecipeSteps.id) &&
        hasValue(RecipeSteps.title) &&
        hasValue(RecipeSteps.body) &&
        hasValue(RecipeSteps.recipe_id) &&
        hasValue(RecipeSteps.number) &&
        hasValue(RecipeSteps.image_id)
    ) {
        StepSearchResponse(
            get(RecipeSteps.id),
            get(RecipeSteps.title),
            get(RecipeSteps.body),
            get(RecipeSteps.recipe_id),
            get(RecipeSteps.number),
            get(RecipeSteps.image_id)
        )
    } else { null }
}
