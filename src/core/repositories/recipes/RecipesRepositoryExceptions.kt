package core.repositories.recipes

import java.lang.Exception

class RecipeUnableMapRawRecipeException(message: String? = null) : Exception(message)

class RecipeUnableCreateNewRecipeException(message: String? = null) : Exception(message)

class RecipeUnknownException(message: String? = null) : Exception(message)

class StepAlreadyExistsException(message: String? = null) : Exception(message) {
    override fun getLocalizedMessage(): String {
        return "Step with current recipeId and number already exists"
    }
}

class StepUnableMapRawStepException(message: String? = null) : Exception(message)

class StepUnableCreateNewStepException(message: String? = null) : Exception(message)

class StepUnknownException(message: String? = null) : Exception(message)
