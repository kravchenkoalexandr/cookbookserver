package core.repositories.recipes

import core.repositories.recipes.requests.RecipeAddRequest
import core.repositories.recipes.requests.RecipeUpdateRequest
import core.repositories.recipes.requests.StepAddRequest
import core.repositories.recipes.requests.StepUpdateRequest
import core.repositories.recipes.responses.RecipeSearchResponse
import core.repositories.recipes.responses.StepSearchResponse

interface RecipesRepository {

    suspend fun addRecipe(recipeAddRequest: RecipeAddRequest): Int

    suspend fun updateRecipe(recipeUpdateRequest: RecipeUpdateRequest): Int

    suspend fun deleteRecipe(id: Int): Int

    suspend fun getRecipeById(id: Int): RecipeSearchResponse?

    suspend fun getRecipes(query: String, page: Int = 0, pageSize: Int = 100): List<RecipeSearchResponse>

    suspend fun addStep(stepAddRequest: StepAddRequest): Int

    suspend fun updateStep(stepUpdateRequest: StepUpdateRequest): Int

    suspend fun deleteStep(id: Int): Int

    suspend fun getStepById(id: Int): StepSearchResponse?

    suspend fun getRecipeSteps(id: Int): List<StepSearchResponse>?

    suspend fun getUserRecipes(userId: Int, page: Int, pageSize: Int): List<RecipeSearchResponse>
}
