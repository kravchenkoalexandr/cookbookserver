package core.repositories.recipes.requests

data class StepUpdateRequest(
    val id: Int,
    val recipeId: Int,
    val title: String,
    val body: String,
    val number: Int,
    val imageId: Int?
)
