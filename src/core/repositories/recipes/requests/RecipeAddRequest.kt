package core.repositories.recipes.requests

import core.repositories.nationalCuisines.NationalCuisinesUnableMapRawNationalCuisinesException

typealias ServiceRecipeAddRequest = core.services.recipes.requests.RecipeAddRequest
typealias ServiceRecipeAddRequestChef = core.services.recipes.requests.RecipeAddRequest.Chef
typealias ServiceRecipeAddRequestNationalCuisine = core.services.recipes.requests.RecipeAddRequest.NationalCuisine
typealias ServiceRecipeStepAddRequest = core.services.recipes.requests.RecipeAddRequest.Step

data class RecipeAddRequest(
    val title: String,
    val description: String?,
    val authorId: Int,
    val steps: List<Step>,
    val imageId: Int?,
    val chefId: Int?,
    val nationalCuisinesId: Int?
) {
    data class Step(
        val title: String,
        val body: String,
        val number: Int,
        val imageId: Int?
    ) {

        object ModelMapper {
            fun from(source: ServiceRecipeStepAddRequest): Step {
                val (title, body, number, imageId) = source

                return Step(title, body, number, imageId)
            }
        }
    }

    data class NationalCuisine(val id: Int?, val name: String)

    object ModelMapper {
        fun from(source: ServiceRecipeAddRequest, chefId: Int?, nationalCuisinesId: Int?): RecipeAddRequest {
            val (title, description, authorId, steps, imageId, _, _) = source

            return RecipeAddRequest(title, description, authorId, steps.map { Step.ModelMapper.from(it) }, imageId,
                chefId, nationalCuisinesId)
        }
    }
}
