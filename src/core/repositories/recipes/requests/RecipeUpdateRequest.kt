package core.repositories.recipes.requests

typealias ServiceRecipeUpdateRequest = core.services.recipes.requests.RecipeUpdateRequest
typealias ServiceRecipeStepUpdateRequest = core.services.recipes.requests.RecipeUpdateRequest.Step

data class RecipeUpdateRequest(
    val id: Int,
    val title: String,
    val description: String?,
    val authorId: Int,
    val steps: List<Step>,
    val imageId: Int?,
    val chefId: Int?,
    val nationalCuisinesId: Int?
) {
    data class Step(
        val id: Int?,
        val title: String,
        val body: String,
        val number: Int,
        val imageId: Int?
    ) {

        object ModelMapper {
            fun from(source: ServiceRecipeStepUpdateRequest): Step {
                val (id, title, body, number, imageId) = source

                return Step(id, title, body, number, imageId)
            }
        }
    }

    object ModelMapper {
        fun from(source: ServiceRecipeUpdateRequest, chefId: Int?, nationalCuisinesId: Int?): RecipeUpdateRequest {
            val (id, title, description, authorId, steps, imageId, _, _) = source

            return RecipeUpdateRequest(id, title, description, authorId, steps.map { Step.ModelMapper.from(it) },
                imageId, chefId, nationalCuisinesId)
        }
    }
}
