package core.repositories.recipes.requests

data class StepAddRequest(
    val recipeId: Int,
    val title: String,
    val body: String,
    val number: Int,
    val imageId: Int?
)
