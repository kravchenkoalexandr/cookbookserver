package core.repositories.recipes

import core.db.models.RecipeSteps
import core.db.models.Recipes
import core.helpers.executeTransaction
import core.repositories.recipes.requests.RecipeAddRequest
import core.repositories.recipes.requests.RecipeUpdateRequest
import core.repositories.recipes.requests.StepAddRequest
import core.repositories.recipes.requests.StepUpdateRequest
import core.repositories.recipes.responses.RecipeSearchResponse
import core.repositories.recipes.responses.StepSearchResponse
import core.repositories.recipes.responses.toRecipe
import core.repositories.recipes.responses.toStep
import kotlinx.coroutines.Dispatchers
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.update

class RecipesRepositoryImpl : RecipesRepository {

    override suspend fun addRecipe(recipeAddRequest: RecipeAddRequest): Int {
        val (title, description, authorId, steps, imageId, chefId, nationalCuisinesId) = recipeAddRequest

        return newSuspendedTransaction {
            val rawId = Recipes.insert {
                it[Recipes.title] = title
                it[Recipes.description] = description
                it[Recipes.author_id] = authorId
                it[Recipes.image_id] = imageId
                it[Recipes.chef_id] = chefId
                it[Recipes.national_cuisines_id] = nationalCuisinesId
            } get Recipes.id

            steps.forEach { (stepTitle, body, number, stepImageId) ->
                addStep(StepAddRequest(rawId, stepTitle, body, number, stepImageId))
            }

            return@newSuspendedTransaction rawId
        }
    }

    override suspend fun updateRecipe(recipeUpdateRequest: RecipeUpdateRequest): Int {
        val (recipeId, title, description, authorId, steps, imageId, chefId, nationalCuisinesId) = recipeUpdateRequest

        return newSuspendedTransaction(Dispatchers.IO) {
            val rawUpdated = Recipes.update({ Recipes.id eq recipeId }) {
                it[Recipes.title] = title
                it[Recipes.description] = description
                it[Recipes.author_id] = authorId
                it[Recipes.image_id] = imageId
                it[Recipes.chef_id] = chefId
                it[Recipes.national_cuisines_id] = nationalCuisinesId
            }

            val oldSteps = getRecipeSteps(recipeId) ?: throw RecipeUnknownException()
            oldSteps.forEach { oldStep ->
                if (steps.none { it.id == oldStep.id }) {
                    println(deleteStep(oldStep.id))
                }
            }

            steps.forEach { (id, stepTitle, body, number, stepImageId) ->
                if (id == null) {
                    addStep(StepAddRequest(recipeId, title, body, number, imageId))
                } else {
                    updateStep(StepUpdateRequest(id, recipeId, stepTitle, body, number, stepImageId))
                }
            }

            return@newSuspendedTransaction rawUpdated
        }
    }

    override suspend fun deleteRecipe(id: Int): Int {
        return newSuspendedTransaction(Dispatchers.IO) {
            return@newSuspendedTransaction Recipes.deleteWhere { Recipes.id eq id }
        }
    }

    override suspend fun getRecipeById(id: Int): RecipeSearchResponse? {
        return newSuspendedTransaction(Dispatchers.IO) {
            val rawRecipe = Recipes.select {
                Recipes.id eq id
            }.singleOrNull() ?: return@newSuspendedTransaction null

            return@newSuspendedTransaction rawRecipe.toRecipe() ?: throw RecipeUnableMapRawRecipeException()
        }
    }

    override suspend fun getRecipes(query: String, page: Int, pageSize: Int): List<RecipeSearchResponse> {
        return executeTransaction {
            return@executeTransaction Recipes
                .select { Recipes.title like "%$query%" }
                .limit(pageSize, page * pageSize)
                .map { it.toRecipe() ?: throw RecipeUnableMapRawRecipeException() }
        }
    }

    override suspend fun addStep(stepAddRequest: StepAddRequest): Int {
        val (recipeId, title, body, number, imageId) = stepAddRequest

        return executeTransaction(mapper = RecipesRepositoryMappers.createStep) {
            val rawId = RecipeSteps.insert {
                it[RecipeSteps.title] = title
                it[RecipeSteps.body] = body
                it[RecipeSteps.recipe_id] = recipeId
                it[RecipeSteps.number] = number
                it[RecipeSteps.image_id] = imageId
            } get RecipeSteps.id

            return@executeTransaction rawId
        }
    }

    override suspend fun updateStep(stepUpdateRequest: StepUpdateRequest): Int {
        val (id, recipeId, title, body, number, imageId) = stepUpdateRequest

        return newSuspendedTransaction(Dispatchers.IO) {
            val rawUpdated = RecipeSteps.update({ RecipeSteps.id eq id }) {
                it[RecipeSteps.title] = title
                it[RecipeSteps.body] = body
                it[RecipeSteps.recipe_id] = recipeId
                it[RecipeSteps.number] = number
                it[RecipeSteps.image_id] = imageId
            }

            return@newSuspendedTransaction rawUpdated
        }
    }

    override suspend fun deleteStep(id: Int): Int {
        return executeTransaction {
            return@executeTransaction RecipeSteps.deleteWhere { RecipeSteps.id eq id }
        }
    }

    override suspend fun getStepById(id: Int): StepSearchResponse? {
        return executeTransaction(mapper = RecipesRepositoryMappers.searchStep) {
            val rawStep = RecipeSteps.select {
                RecipeSteps.id eq id
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawStep.toStep() ?: throw StepUnableMapRawStepException()
        }
    }

    override suspend fun getRecipeSteps(id: Int): List<StepSearchResponse>? {
        return newSuspendedTransaction(Dispatchers.IO) {
            Recipes.select {
                Recipes.id eq id
            }.singleOrNull() ?: return@newSuspendedTransaction null

            return@newSuspendedTransaction RecipeSteps
                .select { RecipeSteps.recipe_id eq id }
                .map { it.toStep() ?: throw StepUnableMapRawStepException() }
        }
    }

    override suspend fun getUserRecipes(userId: Int, page: Int, pageSize: Int): List<RecipeSearchResponse> {
        return executeTransaction {
            return@executeTransaction Recipes
                .select { Recipes.author_id eq userId }
                .limit(pageSize, page)
                .map { it.toRecipe() ?: throw RecipeUnableMapRawRecipeException() }
        }
    }
}
