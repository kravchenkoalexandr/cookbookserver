package core.repositories.users

import java.lang.Exception

class UserAlreadyExistsException(message: String? = null) : Exception(message) {
    override fun getLocalizedMessage(): String {
        return "User already exists"
    }
}

class UserUnableMapRawUserException(message: String? = null) : Exception(message)

class UserUnableCreateNewUserException(message: String? = null) : Exception(message)

class UserUnknownException(message: String? = null) : Exception(message)
