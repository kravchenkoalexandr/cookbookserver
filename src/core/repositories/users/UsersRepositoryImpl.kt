package core.repositories.users

import core.db.models.Users
import core.helpers.executeTransaction
import core.repositories.users.requests.UserAddRequest
import core.repositories.users.responses.UserSearchResponse
import core.repositories.users.responses.toUser
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select

class UsersRepositoryImpl : UsersRepository {

    override suspend fun addUser(userAddRequest: UserAddRequest): Int {
        val (name, email, password, accessToken) = userAddRequest

        return executeTransaction(mapper = UsersRepositoryMappers.creation) {
            val rawId = Users.insert {
                it[Users.name] = name
                it[Users.email] = email
                it[Users.password] = password
                it[Users.accessToken] = accessToken
            } get Users.id

            return@executeTransaction rawId
        }
    }

    override suspend fun getUserById(id: Int): UserSearchResponse? {
        return executeTransaction(mapper = UsersRepositoryMappers.search) {
            val rawUser = Users.select {
                Users.id eq id
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawUser.toUser() ?: throw UserUnableMapRawUserException()
        }
    }

    override suspend fun getUserByEmail(email: String): UserSearchResponse? {
        return executeTransaction(mapper = UsersRepositoryMappers.search) {
            val rawUser = Users.select {
                Users.email eq email
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawUser.toUser() ?: throw UserUnableMapRawUserException()
        }
    }

    override suspend fun getUserByName(name: String): UserSearchResponse? {
        return executeTransaction(mapper = UsersRepositoryMappers.search) {
            val rawUser = Users.select {
                Users.name eq name
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawUser.toUser() ?: throw UserUnableMapRawUserException()
        }
    }
}
