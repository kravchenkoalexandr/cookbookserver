package core.repositories.users.requests

data class UserAddRequest(
    val name: String,
    val email: String,
    val password: String,
    val accessToken: String
)
