package core.repositories.users.responses

import core.db.models.Users
import org.jetbrains.exposed.sql.ResultRow

data class UserSearchResponse(
    val id: Int,
    val name: String,
    val email: String,
    val password: String,
    val accessToken: String
)

fun ResultRow.toUser(): UserSearchResponse? {
    return if (hasValue(Users.id) &&
        hasValue(Users.name) &&
        hasValue(Users.email) &&
        hasValue(Users.password) &&
        hasValue(Users.accessToken)
    ) {
        UserSearchResponse(get(Users.id), get(Users.name), get(Users.email), get(Users.password), get(Users.accessToken))
    } else { null }
}
