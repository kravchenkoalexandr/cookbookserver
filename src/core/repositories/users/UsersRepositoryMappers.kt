package core.repositories.users

import core.helpers.ExposedSQLExceptionsMapper

object UsersRepositoryMappers {
    val creation: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            "23505" -> UserAlreadyExistsException(exception.message)
            else -> UserUnknownException(exception.message)
        }
    }

    val search: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> UserUnknownException(exception.message)
        }
    }
}
