package core.repositories.users

import core.repositories.users.requests.UserAddRequest
import core.repositories.users.responses.UserSearchResponse

interface UsersRepository {

    suspend fun getUserById(id: Int): UserSearchResponse?

    suspend fun getUserByEmail(email: String): UserSearchResponse?

    suspend fun getUserByName(name: String): UserSearchResponse?

    suspend fun addUser(userAddRequest: UserAddRequest): Int
}
