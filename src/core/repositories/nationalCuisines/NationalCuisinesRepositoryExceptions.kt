package core.repositories.nationalCuisines

import java.lang.Exception

class NationalCuisinesUnableMapRawNationalCuisinesException(message: String? = null) : Exception(message)

class NationalCuisinesUnableCreateNewNationalCuisinesException(message: String? = null) : Exception(message)

class NationalCuisinesUnknownException(message: String? = null) : Exception(message)

class NationalCuisinesAlreadyExistsException(message: String? = null) : Exception(message) {
    override fun getLocalizedMessage(): String {
        return "National Cuisines with current name already exists"
    }
}