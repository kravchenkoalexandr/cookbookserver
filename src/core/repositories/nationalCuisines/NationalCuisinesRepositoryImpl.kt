package core.repositories.nationalCuisines

import core.db.models.Chefs
import core.db.models.NationalCuisines
import core.helpers.executeTransaction
import core.repositories.chefs.ChefUnableMapRawChefException
import core.repositories.chefs.ChefsRepositoryMappers
import core.repositories.chefs.responses.toChef
import core.repositories.nationalCuisines.requests.NationalCuisinesAddRequest
import core.repositories.nationalCuisines.responses.NationalCuisinesSearchResponse
import core.repositories.nationalCuisines.responses.toNationalCuisines
import org.jetbrains.exposed.sql.SqlExpressionBuilder.eq
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

class NationalCuisinesRepositoryImpl : NationalCuisinesRepository {

    override suspend fun getNationalCuisineById(id: Int): NationalCuisinesSearchResponse? {
        return executeTransaction(mapper = NationalCuisinesRepositoryMappers.searchNationalCuisine) {
            val rawNationalCuisine = NationalCuisines.select {
                NationalCuisines.id eq id
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawNationalCuisine.toNationalCuisines()
                ?: throw NationalCuisinesUnableMapRawNationalCuisinesException()
        }
    }

    override suspend fun getNationalCuisineByName(name: String): NationalCuisinesSearchResponse? {
        return executeTransaction(mapper = NationalCuisinesRepositoryMappers.searchNationalCuisine) {
            val rawNationalCuisine = NationalCuisines.select {
                NationalCuisines.name eq name
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawNationalCuisine.toNationalCuisines()
                ?: throw NationalCuisinesUnableMapRawNationalCuisinesException()
        }
    }

    override suspend fun getAll(): List<NationalCuisinesSearchResponse> {
        return executeTransaction(mapper = NationalCuisinesRepositoryMappers.searchNationalCuisine) {
            val rawNationalCuisines = NationalCuisines.selectAll()

            return@executeTransaction rawNationalCuisines.map {
                it.toNationalCuisines() ?: throw NationalCuisinesUnableMapRawNationalCuisinesException()
            }
        }
    }

    override suspend fun addNationalCuisine(nationalCuisinesAddRequest: NationalCuisinesAddRequest): Int {
        return executeTransaction(mapper = NationalCuisinesRepositoryMappers.createNationalCuisine) {
            val rawId = NationalCuisines.insert {
                it[NationalCuisines.name] = nationalCuisinesAddRequest.name
            } get NationalCuisines.id

            return@executeTransaction rawId
        }
    }

    override suspend fun addIfNotExists(nationalCuisinesAddRequest: NationalCuisinesAddRequest): Int {
        return newSuspendedTransaction {
            return@newSuspendedTransaction getNationalCuisineByName(nationalCuisinesAddRequest.name)?.id
                ?: addNationalCuisine(nationalCuisinesAddRequest)
        }
    }
}