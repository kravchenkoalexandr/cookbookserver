package core.repositories.nationalCuisines.responses

import core.db.models.NationalCuisines
import org.jetbrains.exposed.sql.ResultRow

data class NationalCuisinesSearchResponse(val id: Int, val name: String)

fun ResultRow.toNationalCuisines(): NationalCuisinesSearchResponse? {
    return if (hasValue(NationalCuisines.id) && hasValue(NationalCuisines.name)) {
        NationalCuisinesSearchResponse(get(NationalCuisines.id), get(NationalCuisines.name))
    } else { null }
}
