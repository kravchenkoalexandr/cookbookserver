package core.repositories.nationalCuisines

import core.helpers.ExposedSQLExceptionsMapper

object NationalCuisinesRepositoryMappers {
    val searchNationalCuisine: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> NationalCuisinesUnknownException(exception.message)
        }
    }

    val createNationalCuisine: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            "23505" -> NationalCuisinesAlreadyExistsException(exception.message)
            else -> NationalCuisinesUnknownException(exception.message)
        }
    }
}
