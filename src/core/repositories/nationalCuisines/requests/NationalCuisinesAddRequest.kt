package core.repositories.nationalCuisines.requests

data class NationalCuisinesAddRequest(val name: String)