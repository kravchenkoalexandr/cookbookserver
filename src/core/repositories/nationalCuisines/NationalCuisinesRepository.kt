package core.repositories.nationalCuisines

import core.repositories.chefs.requests.ChefAddRequest
import core.repositories.chefs.responses.ChefSearchResponse
import core.repositories.nationalCuisines.requests.NationalCuisinesAddRequest
import core.repositories.nationalCuisines.responses.NationalCuisinesSearchResponse

interface NationalCuisinesRepository {

    suspend fun getNationalCuisineById(id: Int): NationalCuisinesSearchResponse?

    suspend fun getNationalCuisineByName(name: String): NationalCuisinesSearchResponse?

    suspend fun addNationalCuisine(nationalCuisinesAddRequest: NationalCuisinesAddRequest): Int

    suspend fun getAll(): List<NationalCuisinesSearchResponse>

    suspend fun addIfNotExists(nationalCuisinesAddRequest: NationalCuisinesAddRequest): Int
}