package core.repositories.chefs

import core.helpers.ExposedSQLExceptionsMapper

object ChefsRepositoryMappers {
    val searchChef: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> ChefUnknownException(exception.message)
        }
    }

    val createChef: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            "23505" -> ChefAlreadyExistsException(exception.message)
            else -> ChefUnknownException(exception.message)
        }
    }
}
