package core.repositories.chefs.requests

data class ChefAddRequest(val name: String)