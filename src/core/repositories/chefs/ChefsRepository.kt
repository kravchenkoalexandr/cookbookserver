package core.repositories.chefs

import core.repositories.chefs.requests.ChefAddRequest
import core.repositories.chefs.responses.ChefSearchResponse

interface ChefsRepository {

    suspend fun getChefById(id: Int): ChefSearchResponse?

    suspend fun getChefByName(fullName: String): ChefSearchResponse?

    suspend fun addChef(chefAddRequest: ChefAddRequest): Int

    suspend fun addIfNotExists(chefAddRequest: ChefAddRequest): Int

    suspend fun getAll(): List<ChefSearchResponse>
}