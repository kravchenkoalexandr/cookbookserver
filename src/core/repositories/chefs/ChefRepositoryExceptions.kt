package core.repositories.chefs

import java.lang.Exception

class ChefUnableMapRawChefException(message: String? = null) : Exception(message)

class ChefUnableCreateNewChefException(message: String? = null) : Exception(message)

class ChefUnknownException(message: String? = null) : Exception(message)

class ChefAlreadyExistsException(message: String? = null) : Exception(message) {
    override fun getLocalizedMessage(): String {
        return "Chef with current name already exists"
    }
}