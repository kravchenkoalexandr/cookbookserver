package core.repositories.chefs.responses

import core.db.models.Chefs
import org.jetbrains.exposed.sql.ResultRow

data class ChefSearchResponse(val id: Int, val fullName: String)

fun ResultRow.toChef(): ChefSearchResponse? {
    return if (hasValue(Chefs.id) && hasValue(Chefs.full_name)) {
        ChefSearchResponse(get(Chefs.id), get(Chefs.full_name))
    } else { null }
}
