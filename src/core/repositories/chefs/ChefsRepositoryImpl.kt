package core.repositories.chefs

import core.db.models.Chefs
import core.helpers.executeTransaction
import core.repositories.chefs.requests.ChefAddRequest
import core.repositories.chefs.responses.ChefSearchResponse
import core.repositories.chefs.responses.toChef
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction

class ChefsRepositoryImpl : ChefsRepository {

    override suspend fun getChefById(id: Int): ChefSearchResponse? {
        return executeTransaction(mapper = ChefsRepositoryMappers.searchChef) {
            val rawChef = Chefs.select {
                Chefs.id eq id
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawChef.toChef() ?: throw ChefUnableMapRawChefException()
        }
    }

    override suspend fun getChefByName(fullName: String): ChefSearchResponse? {
        return executeTransaction(mapper = ChefsRepositoryMappers.searchChef) {
            val rawChef = Chefs.select {
                Chefs.full_name eq fullName
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawChef.toChef() ?: throw ChefUnableMapRawChefException()
        }
    }

    override suspend fun getAll(): List<ChefSearchResponse> {
        return executeTransaction(mapper = ChefsRepositoryMappers.searchChef) {
            val rawChef = Chefs.selectAll()

            return@executeTransaction rawChef.map { it.toChef() ?: throw ChefUnableMapRawChefException() }
        }
    }

    override suspend fun addChef(chefAddRequest: ChefAddRequest): Int {
        return executeTransaction(mapper = ChefsRepositoryMappers.createChef) {
            val rawId = Chefs.insert {
                it[Chefs.full_name] = chefAddRequest.name
            } get Chefs.id

            return@executeTransaction rawId
        }
    }

    override suspend fun addIfNotExists(chefAddRequest: ChefAddRequest): Int {
        return newSuspendedTransaction {
            return@newSuspendedTransaction getChefByName(chefAddRequest.name)?.id ?: addChef(chefAddRequest)
        }
    }
}