package core.repositories.images

import core.db.models.Images
import core.helpers.deleteIfExists
import core.helpers.executeTransaction
import core.helpers.saveFile
import core.repositories.images.requests.ImageAddRequest
import core.repositories.images.responses.ImageSearchResponse
import core.repositories.images.responses.toImageInfo
import java.io.File
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.insert
import org.jetbrains.exposed.sql.select

class ImagesRepositoryImpl : ImagesRepository {

    override suspend fun addImage(imageAddRequest: ImageAddRequest): Int {
        val (path, name, imageSource) = imageAddRequest

        saveFile(path, name, imageSource)
        try {
            return executeTransaction(mapper = ImagesRepositoryMappers.creation) {
                val rawId = Images.insert {
                    it[Images.path] = path
                    it[Images.name] = name
                } get Images.id

                return@executeTransaction rawId
            }
        } catch (exception: Exception) {
            File(path + File.separator + name).deleteIfExists()

            throw exception
        }
    }

    override suspend fun getImageById(id: Int): ImageSearchResponse? {
        val imageInfo = executeTransaction(mapper = ImagesRepositoryMappers.search) {
            val rawImage = Images.select {
                Images.id eq id
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawImage.toImageInfo() ?: throw ImageUnableMapRawImageInfoException()
        } ?: return null

        val image = File(imageInfo.path + File.separator + imageInfo.name)

        return if (image.exists() && image.isFile) {
            ImageSearchResponse(imageInfo, image)
        } else { null }
    }

    override suspend fun getImage(path: String, name: String): ImageSearchResponse? {
        val imageInfo = executeTransaction(mapper = ImagesRepositoryMappers.search) {
            val rawImage = Images.select {
                (Images.path eq path) and (Images.name eq name)
            }.singleOrNull() ?: return@executeTransaction null

            return@executeTransaction rawImage.toImageInfo() ?: throw ImageUnableMapRawImageInfoException()
        } ?: return null

        val image = File(imageInfo.path + File.separator + imageInfo.name)

        return if (image.exists() && image.isFile) {
            ImageSearchResponse(imageInfo, image)
        } else { null }
    }
}
