package core.repositories.images

import core.helpers.ExposedSQLExceptionsMapper

object ImagesRepositoryMappers {

    val creation: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            "23505" -> ImageAlreadyExistsException(exception.message)
            else -> ImageUnknownException(exception.message)
        }
    }

    val search: ExposedSQLExceptionsMapper = { exception ->
        when (exception.sqlState) {
            else -> ImageUnknownException(exception.message)
        }
    }
}
