package core.repositories.images

import core.repositories.images.requests.ImageAddRequest
import core.repositories.images.responses.ImageSearchResponse

interface ImagesRepository {

    suspend fun addImage(imageAddRequest: ImageAddRequest): Int

    suspend fun getImageById(id: Int): ImageSearchResponse?

    suspend fun getImage(path: String, name: String): ImageSearchResponse?
}
