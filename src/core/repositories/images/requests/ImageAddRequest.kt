package core.repositories.images.requests

import java.io.InputStream

data class ImageAddRequest(
    val path: String,
    val name: String,
    val imageSource: InputStream
)
