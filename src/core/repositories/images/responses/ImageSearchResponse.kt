package core.repositories.images.responses

import java.io.File

data class ImageSearchResponse(
    val imageInfo: ImageInfo,
    val image: File
) {
    constructor(id: Int, path: String, name: String, image: File) : this(ImageInfo(id, path, name), image)
}
