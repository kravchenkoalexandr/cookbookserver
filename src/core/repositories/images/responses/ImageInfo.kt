package core.repositories.images.responses

import core.db.models.Images
import org.jetbrains.exposed.sql.ResultRow

data class ImageInfo(
    val id: Int,
    val path: String,
    val name: String
)

fun ResultRow.toImageInfo(): ImageInfo? {
    return if (hasValue(Images.id) && hasValue(Images.path) && hasValue(Images.name)) {
        ImageInfo(get(Images.id), get(Images.path), get(Images.name))
    } else { null }
}
