package core.repositories.images

import java.lang.Exception

class ImageAlreadyExistsException(message: String? = null) : Exception(message) {
    override fun getLocalizedMessage(): String {
        return "Image with current path and name already exists"
    }
}

class ImageUnableMapRawImageInfoException(message: String? = null) : Exception(message)

class ImageUnableCreateNewImageException(message: String? = null) : Exception(message)

class ImageUnknownException(message: String? = null) : Exception(message)
