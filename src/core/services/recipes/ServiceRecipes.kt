package core.services.recipes

import core.services.recipes.requests.RecipeAddRequest
import core.services.recipes.requests.RecipeUpdateRequest
import core.services.recipes.responses.RecipeResponse

interface ServiceRecipes {

    suspend fun addRecipe(recipeAddRequest: RecipeAddRequest): RecipeResponse

    suspend fun updateRecipe(recipeUpdateRequest: RecipeUpdateRequest): RecipeResponse

    suspend fun deleteRecipe(id: Int): RecipeResponse?

    suspend fun getRecipeById(id: Int): RecipeResponse?

    suspend fun getRecipe(query: String, page: Int, pageSize: Int): List<RecipeResponse>

    suspend fun getUserRecipes(userId: Int, page: Int, pageSize: Int): List<RecipeResponse>
}
