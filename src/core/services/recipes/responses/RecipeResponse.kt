package core.services.recipes.responses

import core.repositories.chefs.responses.ChefSearchResponse
import core.repositories.nationalCuisines.responses.NationalCuisinesSearchResponse
import core.repositories.recipes.responses.RecipeSearchResponse
import core.repositories.recipes.responses.StepSearchResponse
import core.repositories.users.responses.UserSearchResponse

data class RecipeResponse(
    val id: Int,
    val title: String,
    val description: String?,
    val author: Author,
    val likesCount: Int,
    val stepsCount: Int,
    val steps: List<Step>,
    val imageUrl: String?,
    val chef: Chef?,
    val nationalCuisine: NationalCuisine?
) {

    data class Author(val id: Int, val name: String) {

        object ModelMapper {
            fun from(source: UserSearchResponse): Author {
                val (id, name, _, _, _) = source
                return Author(id, name)
            }
        }
    }

    data class Chef(val id: Int, val fullName: String) {

        object ModelMapper {
            fun from(source: ChefSearchResponse): Chef {
                val (id, fullName) = source

                return Chef(id, fullName)
            }
        }
    }

    data class NationalCuisine(val id: Int, val name: String) {

        object ModelMapper {
            fun from(source: NationalCuisinesSearchResponse): NationalCuisine {
                val (id, name) = source

                return NationalCuisine(id, name)
            }
        }
    }

    data class Step(val id: Int, val title: String, val body: String, val recipeId: Int, val number: Int,
                    val imageUrl: String?) {

        object ModelMapper {
            fun from(source: StepSearchResponse, imageUrl: String?): Step {
                val (id, title, body, recipeId, number, _) = source

                return Step(id, title, body, recipeId, number, imageUrl)
            }
        }
    }

    object ModelMapper {
        fun from(
            source: RecipeSearchResponse,
            author: Author,
            steps: List<StepSearchResponse>,
            mapSteps: (StepSearchResponse) -> Step,
            imageUrl: String?,
            chef: Chef?,
            nationalCuisine: NationalCuisine?
        ): RecipeResponse {
            val (id, title, description, _, stepsCount, likesCount, _, _, _) = source

            return RecipeResponse(id, title, description, author, likesCount, stepsCount, steps.map(mapSteps), imageUrl,
                chef, nationalCuisine)
        }
    }
}
