package core.services.recipes.requests

typealias ModuleRecipeAddRequest = modules.recipes.requests.RecipeAddRequest
typealias ModuleRecipeAddRequestChef = modules.recipes.requests.RecipeAddRequest.Chef
typealias ModuleRecipeAddRequestNationalCuisine = modules.recipes.requests.RecipeAddRequest.NationalCuisine
typealias ModuleRecipeStepAddRequest = modules.recipes.requests.RecipeAddRequest.Step

data class RecipeAddRequest(
    val title: String,
    val description: String?,
    val authorId: Int,
    val steps: List<Step>,
    val imageId: Int?,
    val chef: Chef?,
    val nationalCuisine: NationalCuisine?
) {

    data class Step(
        val title: String,
        val body: String,
        val number: Int,
        val imageId: Int?
    ) {

        object ModelMapper {
            fun from(source: ModuleRecipeStepAddRequest, imageId: Int?): Step {
                val (title, body, number, _) = source

                return Step(title, body, number, imageId)
            }
        }
    }

    data class Chef(val id: Int?, val fullName: String) {

        object ModelMapper {
            fun from(source: ModuleRecipeAddRequestChef): Chef {
                val (id, fullName) = source

                return Chef(id, fullName)
            }
         }
    }

    data class NationalCuisine(val id: Int?, val name: String) {

        object ModelMapper {
            fun from(source: ModuleRecipeAddRequestNationalCuisine): NationalCuisine {
                val (id, name) = source

                return NationalCuisine(id, name)
            }
        }
    }

    object ModelMapper {
        fun from(
            source: ModuleRecipeAddRequest,
            authorId: Int,
            mapStep: (ModuleRecipeStepAddRequest) -> Step,
            imageId: Int?
        ): RecipeAddRequest {
            val (title, description, steps, _, chef, nationalCuisine) = source
            val chefMapped = if (chef != null) { Chef.ModelMapper.from(chef) } else { null }
            val nationalCuisineMapped = if (nationalCuisine != null) { NationalCuisine.ModelMapper.from(nationalCuisine) } else { null }

            return RecipeAddRequest(title, description, authorId, steps.map(mapStep), imageId, chefMapped, nationalCuisineMapped)
        }
    }
}
