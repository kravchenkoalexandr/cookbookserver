package core.services.recipes.requests

typealias ModuleRecipeUpdateRequest = modules.recipes.requests.RecipeUpdateRequest
typealias ModuleRecipeStepUpdateRequest = modules.recipes.requests.RecipeUpdateRequest.Step
typealias ModuleRecipeUpdateRequestChef = modules.recipes.requests.RecipeUpdateRequest.Chef
typealias ModuleRecipeUpdateRequestNationalCuisine = modules.recipes.requests.RecipeUpdateRequest.NationalCuisine

data class RecipeUpdateRequest(
    val id: Int,
    val title: String,
    val description: String?,
    val authorId: Int,
    val steps: List<Step>,
    val imageId: Int?,
    val chef: Chef?,
    val nationalCuisine: NationalCuisine?
) {

    data class Step(
        val id: Int?,
        val title: String,
        val body: String,
        val number: Int,
        val imageId: Int?
    ) {

        object ModelMapper {
            fun from(source: ModuleRecipeStepUpdateRequest, imageId: Int?): Step {
                val (id, title, body, number, _) = source

                return Step(id, title, body, number, imageId)
            }
        }
    }

    data class Chef(val id: Int?, val fullName: String) {

        object ModelMapper {
            fun from(source: ModuleRecipeUpdateRequestChef): Chef {
                val (id, fullName) = source

                return Chef(id, fullName)
            }
        }
    }

    data class NationalCuisine(val id: Int?, val name: String) {

        object ModelMapper {
            fun from(source: ModuleRecipeUpdateRequestNationalCuisine): NationalCuisine {
                val (id, name) = source

                return NationalCuisine(id, name)
            }
        }
    }

    object ModelMapper {
        fun from(
            source: ModuleRecipeUpdateRequest,
            authorId: Int,
            mapStep: (ModuleRecipeStepUpdateRequest) -> Step,
            imageId: Int?
        ): RecipeUpdateRequest {
            val (id, title, description, steps, _, chef, nationalCuisine) = source
            val chefMapped = if (chef != null) { Chef.ModelMapper.from(chef) } else { null }
            val nationalCuisineMapped = if (nationalCuisine != null) { NationalCuisine.ModelMapper.from(nationalCuisine) } else { null }

            return RecipeUpdateRequest(id, title, description, authorId, steps.map(mapStep), imageId, chefMapped, nationalCuisineMapped)
        }
    }
}
