package core.services.recipes

import core.repositories.chefs.ChefsRepository
import core.repositories.chefs.requests.ChefAddRequest
import core.repositories.nationalCuisines.NationalCuisinesRepository
import core.repositories.nationalCuisines.requests.NationalCuisinesAddRequest
import core.repositories.recipes.RecipeUnknownException
import core.repositories.recipes.RecipesRepository
import core.repositories.recipes.responses.StepSearchResponse
import core.repositories.users.UsersRepository
import core.services.images.ServiceImages
import core.services.recipes.requests.RecipeAddRequest
import core.services.recipes.requests.RecipeUpdateRequest
import core.services.recipes.responses.RecipeResponse

typealias RepositoryRecipeAddRequestMapper = core.repositories.recipes.requests.RecipeAddRequest.ModelMapper
typealias RepositoryRecipeUpdateRequestMapper = core.repositories.recipes.requests.RecipeUpdateRequest.ModelMapper

class ServiceRecipesImpl(
    val recipesRepository: RecipesRepository,
    val usersRepository: UsersRepository,
    val serviceImages: ServiceImages,
    val chefsRepository: ChefsRepository,
    val nationalCuisinesRepository: NationalCuisinesRepository
) : ServiceRecipes {

    override suspend fun addRecipe(recipeAddRequest: RecipeAddRequest): RecipeResponse {
        var chefId: Int? = null
        var nationalCuisineId: Int? = null
        recipeAddRequest.chef?.let { chef ->
            chefId = chef.id ?: chefsRepository.addIfNotExists(ChefAddRequest(chef.fullName))
        }
        recipeAddRequest.nationalCuisine?.let { nationalCuisine ->
            nationalCuisineId = nationalCuisine.id ?: nationalCuisinesRepository.addIfNotExists(
                NationalCuisinesAddRequest(nationalCuisine.name)
            )
        }

        val recipeToAdd = RepositoryRecipeAddRequestMapper.from(recipeAddRequest, chefId, nationalCuisineId)
        val recipeId = recipesRepository.addRecipe(recipeToAdd)

        return getRecipeById(recipeId) ?: throw RecipeUnknownException()
    }

    override suspend fun updateRecipe(recipeUpdateRequest: RecipeUpdateRequest): RecipeResponse {
        var chefId: Int? = null
        var nationalCuisineId: Int? = null
        recipeUpdateRequest.chef?.let { chef ->
            chefId = chef.id ?: chefsRepository.addIfNotExists(ChefAddRequest(chef.fullName))
        }
        recipeUpdateRequest.nationalCuisine?.let { nationalCuisine ->
            nationalCuisineId = nationalCuisine.id ?: nationalCuisinesRepository.addIfNotExists(
                NationalCuisinesAddRequest(nationalCuisine.name)
            )
        }

        val recipeToUpdate = RepositoryRecipeUpdateRequestMapper.from(recipeUpdateRequest, chefId, nationalCuisineId)
        val updatedCount = recipesRepository.updateRecipe(recipeToUpdate)

        return getRecipeById(recipeUpdateRequest.id) ?: throw RecipeUnknownException()
    }

    override suspend fun deleteRecipe(id: Int): RecipeResponse? {
        val recipe = getRecipeById(id) ?: return null
        recipesRepository.deleteRecipe(id)

        return recipe
    }

    override suspend fun getRecipeById(id: Int): RecipeResponse? {
        val recipeToResponse = recipesRepository.getRecipeById(id) ?: return null
        val stepsToResponse = recipesRepository.getRecipeSteps(id) ?: throw RecipeUnknownException()
        val userToResponse = usersRepository.getUserById(recipeToResponse.authorId) ?: throw RecipeUnknownException()

        val chefToResponse = if (recipeToResponse.chefId != null) {
            RecipeResponse.Chef.ModelMapper.from(
                chefsRepository.getChefById(recipeToResponse.chefId)
                    ?: throw RecipeUnknownException()
            )
        } else { null }

        val nationalCuisineToResponse = if (recipeToResponse.nationalCuisineId != null) {
            RecipeResponse.NationalCuisine.ModelMapper.from(
                nationalCuisinesRepository.getNationalCuisineById(recipeToResponse.nationalCuisineId)
                    ?: throw RecipeUnknownException()
            )
        } else { null }

        val stepsWithUrls = mutableMapOf<StepSearchResponse, String?>()
        val recipeImageUrl = tryGetImageUrl(serviceImages, recipeToResponse.imageId)
        stepsToResponse.forEach { stepsWithUrls[it] = tryGetImageUrl(serviceImages, it.imageId) }

        return RecipeResponse.ModelMapper.from(
            recipeToResponse,
            RecipeResponse.Author.ModelMapper.from(userToResponse),
            stepsToResponse,
            { RecipeResponse.Step.ModelMapper.from(it, stepsWithUrls[it]) },
            recipeImageUrl,
            chefToResponse,
            nationalCuisineToResponse
        )
    }

    override suspend fun getRecipe(query: String, page: Int, pageSize: Int): List<RecipeResponse> {
        val recipes = recipesRepository.getRecipes(query, page, pageSize)

        return recipes.map { getRecipeById(it.id) ?: throw RecipeUnknownException() }
    }

    override suspend fun getUserRecipes(userId: Int, page: Int, pageSize: Int): List<RecipeResponse> {
        val recipes = recipesRepository.getUserRecipes(userId, page, pageSize)

        return recipes.map { getRecipeById(it.id) ?: throw RecipeUnknownException() }
    }

    private suspend fun tryGetImageUrl(
        serviceImages: ServiceImages,
        imageId: Int?
    ): String? {
        var imageUrl: String? = null
        if (imageId != null) {
            imageUrl = serviceImages.getImageUrlById(imageId)
            if (imageUrl == null) {
                throw RecipeUnknownException()
            }
        }
        return imageUrl
    }
}
