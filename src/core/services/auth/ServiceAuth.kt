package core.services.auth

import core.services.auth.requests.UserLoginRequest
import core.services.auth.requests.UserRegisterRequest
import core.services.auth.responses.UserAuthResponse

interface ServiceAuth {

    suspend fun login(userLoginRequest: UserLoginRequest): UserAuthResponse?

    suspend fun register(userRegisterRequest: UserRegisterRequest): UserAuthResponse
}
