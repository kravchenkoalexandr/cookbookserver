package core.services.auth

import core.repositories.users.UserUnknownException
import core.repositories.users.UsersRepository
import core.repositories.users.requests.UserAddRequest
import core.services.auth.requests.UserLoginRequest
import core.services.auth.requests.UserRegisterRequest
import core.services.auth.responses.UserAuthResponse
import modules.auth.JwtAuth
import org.mindrot.jbcrypt.BCrypt

class ServiceAuthImpl(val repository: UsersRepository) : ServiceAuth {

    override suspend fun login(userLoginRequest: UserLoginRequest): UserAuthResponse? {
        val (email, password) = userLoginRequest

        val user = repository.getUserByEmail(email) ?: return null

        return if (BCrypt.checkpw(password, user.password))
            UserAuthResponse(user.id, user.name, user.email, user.accessToken) else null
    }

    override suspend fun register(userRegisterRequest: UserRegisterRequest): UserAuthResponse {
        val (name, email, password) = userRegisterRequest

        val token = JwtAuth.generateAccessToken(name)
        val hashedPassword = BCrypt.hashpw(password, BCrypt.gensalt(4))

        val id = repository.addUser(UserAddRequest(name, email, hashedPassword, token))
        val user = repository.getUserById(id) ?: throw UserUnknownException("Cannot get new user by id")

        return UserAuthResponse(user.id, user.name, user.email, user.accessToken)
    }
}
