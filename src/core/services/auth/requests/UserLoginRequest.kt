package core.services.auth.requests

data class UserLoginRequest(
    val email: String,
    val password: String
)
