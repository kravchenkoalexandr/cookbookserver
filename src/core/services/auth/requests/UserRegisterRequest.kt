package core.services.auth.requests

data class UserRegisterRequest(
    val name: String,
    val email: String,
    val password: String
)
