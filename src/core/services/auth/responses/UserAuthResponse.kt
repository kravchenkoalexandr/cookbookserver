package core.services.auth.responses

data class UserAuthResponse(
    val id: Int,
    val name: String,
    val email: String,
    val token: String
)
