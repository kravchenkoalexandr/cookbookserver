package core.services.images.responses

import java.io.File

data class ImageSearchResponse(
    val id: Int,
    val path: String,
    val name: String,
    val url: String,
    val image: File
)
