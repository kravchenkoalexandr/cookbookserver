package core.services.images

import core.services.images.requests.ImageAddRequest
import core.services.images.responses.ImageSearchResponse

interface ServiceImages {

    suspend fun addImage(imageAddRequest: ImageAddRequest): Int

    suspend fun getImageById(id: Int): ImageSearchResponse?

    suspend fun getImageByUrl(url: String): ImageSearchResponse?

    suspend fun getImageUrlById(id: Int): String?
}
