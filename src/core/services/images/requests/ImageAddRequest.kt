package core.services.images.requests

import java.io.InputStream

data class ImageAddRequest(
    val name: String,
    val imageSource: InputStream
)
