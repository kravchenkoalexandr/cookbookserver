package core.services.images

import app.AppConfig
import core.repositories.images.ImagesRepository
import core.services.images.requests.ImageAddRequest
import core.services.images.responses.ImageSearchResponse

typealias RepositoryImageAddRequest = core.repositories.images.requests.ImageAddRequest

class ServiceImagesImpl(val imagesRepository: ImagesRepository) : ServiceImages {

    val imagesAddress = AppConfig.address + "/" + AppConfig.Static.Images.route + "/"
    val imagesDirectory = AppConfig.Static.Images.dir

    override suspend fun addImage(imageAddRequest: ImageAddRequest): Int {
        val (name, imageSource) = imageAddRequest

        return imagesRepository.addImage(RepositoryImageAddRequest(imagesDirectory, name, imageSource))
    }

    override suspend fun getImageById(id: Int): ImageSearchResponse? {
        val (imageInfo, image) = imagesRepository.getImageById(id) ?: return null
        val (_, path, name) = imageInfo
        val url = imagesAddress + name

        return ImageSearchResponse(id, path, name, url, image)
    }

    override suspend fun getImageByUrl(url: String): ImageSearchResponse? {
        val extractedName = url.removePrefix(imagesAddress)
        if (extractedName == url || extractedName.isBlank()) {
            return null
        }

        val (imageInfo, image) = imagesRepository.getImage(imagesDirectory, extractedName) ?: return null
        val (id, path, name) = imageInfo

        return ImageSearchResponse(id, path, name, url, image)
    }

    override suspend fun getImageUrlById(id: Int): String? {
        val image = imagesRepository.getImageById(id) ?: return null

        return imagesAddress + image.imageInfo.name
    }
}
