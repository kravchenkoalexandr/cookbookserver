package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object Images : Table("") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val ref_count: Column<Int> = integer("ref_count").default(0)
    val path: Column<String> = varchar("path", 256).check { it.neq("") }
    val name: Column<String> = varchar("name", 256).check { it.neq("") }

    init {
        uniqueIndex(path, name)
    }
}
