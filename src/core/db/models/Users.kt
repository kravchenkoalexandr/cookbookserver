package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object Users : Table("users") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val name: Column<String> = varchar("name", 64).check { it.neq("") }.uniqueIndex()
    val email: Column<String> = varchar("email", 64).check { it.neq("") }.uniqueIndex()
    val password: Column<String> = varchar("password", 100).check { it.neq("") }
    val accessToken: Column<String> = varchar("access_token", 256).check { it.neq("") }.uniqueIndex()
}
