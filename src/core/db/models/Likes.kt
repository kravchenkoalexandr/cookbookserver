package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object Likes : Table("likes") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val owner_id: Column<Int> = integer("owner_id").references(Users.id)
    val recipe_id: Column<Int> = integer("recipe_id").references(Recipes.id)

    init {
        uniqueIndex(owner_id, recipe_id)
    }
}
