package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.SqlExpressionBuilder.greaterEq
import org.jetbrains.exposed.sql.SqlExpressionBuilder.neq
import org.jetbrains.exposed.sql.Table

object Chefs : Table("chefs") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val full_name: Column<String> = varchar("full_name", 256).check { it.neq("") }.uniqueIndex()
}
