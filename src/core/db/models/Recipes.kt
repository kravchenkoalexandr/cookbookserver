package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object Recipes : Table("recipes") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val title: Column<String> = varchar("title", 100).check { it.neq("") }
    val description: Column<String?> = varchar("description", 256).check { it.neq("") }.nullable()
    val author_id: Column<Int> = integer("author_id").references(Users.id)
    val steps_count: Column<Int> = integer("steps_count").default(0)
    val likes_count: Column<Int> = integer("likes_count").default(0)
    val image_id: Column<Int?> = integer("image_id").references(Images.id).nullable()
    val chef_id: Column<Int?> = integer("chef_id").references(Chefs.id).nullable()
    val national_cuisines_id: Column<Int?> = integer("national_cuisines_id").references(NationalCuisines.id).nullable()
}
