package core.db.models

import org.jetbrains.exposed.sql.Column
import org.jetbrains.exposed.sql.Table

object RecipeSteps : Table("recipe_steps") {
    val id: Column<Int> = integer("id").autoIncrement().primaryKey()
    val recipe_id: Column<Int> = integer("recipe_id").references(Recipes.id)
    val number: Column<Int> = integer("number").check { it.greaterEq(0) }
    val title: Column<String> = varchar("title", 100).check { it.neq("") }
    val body: Column<String> = varchar("body", 300).check { it.neq("") }
    val image_id: Column<Int?> = integer("image_id").references(Images.id).nullable()

    init {
        uniqueIndex(recipe_id, number)
    }
}
