package core.base

import com.fasterxml.jackson.annotation.JsonProperty

data class PagingResult<T>(
    @JsonProperty ("items") val items: List<T>,
    @JsonProperty ("totalItems") val totalItems: Int
)