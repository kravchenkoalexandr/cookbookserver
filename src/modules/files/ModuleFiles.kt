package modules.files

import app.AppConfig
import core.helpers.PartDataImagesMap
import core.repositories.images.ImageUnknownException
import core.repositories.images.ImagesRepositoryImpl
import core.services.images.ServiceImagesImpl
import core.services.images.requests.ImageAddRequest
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.files
import io.ktor.http.content.readAllParts
import io.ktor.http.content.static
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import java.util.UUID
import modules.files.responses.UploadResponse

fun Application.moduleFiles() {
    val imagesRepository = ImagesRepositoryImpl()
    val serviceImages = ServiceImagesImpl(imagesRepository)

    routing {
        static(AppConfig.Static.Images.route) {
            files(AppConfig.Static.Images.dir)
        }

        authenticate {
            post("/files/upload") {
                val parts = call.receiveMultipart().readAllParts()
                val imagesMap = PartDataImagesMap(parts)
                if (imagesMap.isEmpty() || imagesMap.size > 1) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@post
                }

                val (_, extension, part) = imagesMap.values.first()
                val name = generateFileName() + extension
                val id = serviceImages.addImage(ImageAddRequest(name, part.streamProvider()))

                parts.forEach { it.dispose() }

                val result = serviceImages.getImageById(id) ?: throw ImageUnknownException()

                call.respond(UploadResponse(result.url))
            }
        }
    }
}

private fun generateFileName(): String {
    return UUID.randomUUID().toString()
}
