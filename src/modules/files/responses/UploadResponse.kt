package modules.files.responses

import com.fasterxml.jackson.annotation.JsonProperty

data class UploadResponse(@JsonProperty("url") val url: String)
