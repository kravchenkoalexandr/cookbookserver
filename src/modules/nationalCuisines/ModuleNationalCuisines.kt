package modules.nationalCuisines

import core.repositories.nationalCuisines.NationalCuisinesRepositoryImpl
import core.repositories.nationalCuisines.responses.NationalCuisinesSearchResponse
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.locations.get
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import modules.nationalCuisines.responses.NationalCuisinesResponse

fun Application.moduleNationalCuisines() {
    val nationalCuisinesRepository = NationalCuisinesRepositoryImpl()

    routing {
        get("/cuisines") {
            call.respond(nationalCuisinesRepository.getAll().map { NationalCuisinesResponse.ModuleMapper.from(it) })
        }
    }
}