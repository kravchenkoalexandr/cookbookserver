package modules.nationalCuisines.responses

import com.fasterxml.jackson.annotation.JsonProperty

typealias NationalCuisinesRepositorySearchResponse = core.repositories.nationalCuisines.responses.NationalCuisinesSearchResponse

data class NationalCuisinesResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("name") val name: String
) {

    object ModuleMapper {
        fun from(source: NationalCuisinesRepositorySearchResponse): NationalCuisinesResponse {
            val (id, name) = source

            return NationalCuisinesResponse(id, name)
        }
    }
}