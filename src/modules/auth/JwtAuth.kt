package modules.auth

import com.auth0.jwt.JWT
import com.auth0.jwt.JWTVerifier
import com.auth0.jwt.algorithms.Algorithm
import app.AppConfig

object JwtAuth {
    val config = AppConfig.Jwt
    private val algorithm = Algorithm.HMAC512(config.secret)

    val verifier: JWTVerifier = JWT
        .require(algorithm)
        .build()

    fun generateAccessToken(name: String): String {
        return JWT.create()
            .withClaim("name", name)
            .sign(algorithm)
    }
}
