package modules.auth.responses

import com.fasterxml.jackson.annotation.JsonProperty

data class AuthResponse(
    @JsonProperty("user") val user: User,
    @JsonProperty("access") val access: Access
) {
    constructor(id: Int, name: String, token: String) : this(User(id, name), Access(token))

    data class User(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String
    )

    data class Access(
        @JsonProperty("token") val token: String
    )
}
