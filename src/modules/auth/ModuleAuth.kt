package modules.auth

import app.UserPrincipal
import core.repositories.users.UsersRepositoryImpl
import core.services.auth.ServiceAuthImpl
import core.services.auth.requests.UserLoginRequest
import core.services.auth.requests.UserRegisterRequest
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.auth.Authentication
import io.ktor.auth.jwt.jwt
import io.ktor.http.HttpStatusCode
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.routing
import modules.auth.requests.LoginRequest
import modules.auth.requests.RegisterRequest
import modules.auth.responses.AuthResponse

fun Application.moduleAuth() {
    val usersRepository = UsersRepositoryImpl()
    val serviceAuth = ServiceAuthImpl(usersRepository)

    install(Authentication) {
        jwt {
            verifier(JwtAuth.verifier)
            realm = JwtAuth.config.realm

            validate {
                val name = it.payload.getClaim("name").asString() ?: return@validate null
                val user = usersRepository.getUserByName(name) ?: return@validate null

                return@validate UserPrincipal(user.id, user.name)
            }
        }
    }

    routing {
        post("/login") {
            val (email, password) = call.receive<LoginRequest>()
            val userLoginRequest = UserLoginRequest(email, password)
            val result = serviceAuth.login(userLoginRequest)

            if (result == null) {
                call.respond(HttpStatusCode.Unauthorized)
            } else {
                call.respond(AuthResponse(result.id, result.name, result.token))
            }
        }

        post("/register") {
            val (name, email, password) = call.receive<RegisterRequest>()
            val userRegisterRequest = UserRegisterRequest(name, email, password)
            val result = serviceAuth.register(userRegisterRequest)

            call.respond(AuthResponse(result.id, result.name, result.token))
        }
    }
}
