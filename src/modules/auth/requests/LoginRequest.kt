package modules.auth.requests

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class LoginRequest(
    @JsonProperty("email") val email: String,
    @JsonProperty("password") val password: String
)
