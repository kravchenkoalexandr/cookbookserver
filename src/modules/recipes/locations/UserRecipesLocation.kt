package modules.recipes.locations

import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location("/users/{id}/recipes")
data class UserRecipesLocation(val id: Int, val page: Int, val pageSize: Int)