package modules.recipes.locations

import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location("/recipes/{id}")
data class RecipeGetLocation(val id: Int)