package modules.recipes.locations

import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Location

@KtorExperimentalLocationsAPI
@Location("/recipes")
data class RecipeSearchLocation(val q: String, val page: Int, val pageSize: Int)
