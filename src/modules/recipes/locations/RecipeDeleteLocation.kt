package modules.recipes.locations

import io.ktor.locations.Location

@Location("/recipes/{id}")
data class RecipeDeleteLocation(val id: Int)
