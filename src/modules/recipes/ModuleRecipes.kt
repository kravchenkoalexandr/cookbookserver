package modules.recipes

import app.UserPrincipal
import app.UserPrincipalNotAvailable
import core.base.PagingResult
import core.repositories.chefs.ChefsRepositoryImpl
import core.repositories.images.ImagesRepositoryImpl
import core.repositories.nationalCuisines.NationalCuisinesRepository
import core.repositories.nationalCuisines.NationalCuisinesRepositoryImpl
import core.repositories.recipes.RecipesRepositoryImpl
import core.repositories.users.UsersRepositoryImpl
import core.services.images.ServiceImages
import core.services.images.ServiceImagesImpl
import core.services.recipes.ServiceRecipesImpl
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.auth.authenticate
import io.ktor.auth.principal
import io.ktor.http.HttpStatusCode
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.delete
import io.ktor.locations.get
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.post
import io.ktor.routing.put
import io.ktor.routing.routing
import modules.recipes.locations.RecipeDeleteLocation
import modules.recipes.locations.RecipeGetLocation
import modules.recipes.locations.RecipeSearchLocation
import modules.recipes.locations.UserRecipesLocation
import modules.recipes.requests.RecipeAddRequest
import modules.recipes.requests.RecipeUpdateRequest
import modules.recipes.responses.RecipeResponse

typealias ServiceRecipeAddRequestMapper = core.services.recipes.requests.RecipeAddRequest.ModelMapper
typealias ServiceRecipeStepAddRequestMapper = core.services.recipes.requests.RecipeAddRequest.Step.ModelMapper

typealias ServiceRecipeUpdateRequestMapper = core.services.recipes.requests.RecipeUpdateRequest.ModelMapper
typealias ServiceRecipeStepUpdateRequestMapper = core.services.recipes.requests.RecipeUpdateRequest.Step.ModelMapper

@KtorExperimentalLocationsAPI
fun Application.moduleRecipes() {
    val usersRepository = UsersRepositoryImpl()
    val imagesRepository = ImagesRepositoryImpl()
    val recipesRepository = RecipesRepositoryImpl()
    val chefsRepository = ChefsRepositoryImpl()
    val nationalCuisinesRepository = NationalCuisinesRepositoryImpl()
    val serviceImages = ServiceImagesImpl(imagesRepository)
    val serviceRecipes = ServiceRecipesImpl(recipesRepository, usersRepository,  serviceImages, chefsRepository,
        nationalCuisinesRepository)

    routing {

        authenticate {

            post("/recipes") {
                val (authorId, _) = call.principal<UserPrincipal>()
                    ?: throw UserPrincipalNotAvailable("(post) /recipes")
                val request = call.receive<RecipeAddRequest>()

                val recipeImageId: Int?
                val stepsWithIds = mutableMapOf<RecipeAddRequest.Step, Int?>()
                try {
                    recipeImageId = tryGetImageId(serviceImages, request.imageUrl)
                    request.steps.forEach { stepsWithIds[it] = tryGetImageId(serviceImages, it.imageUrl) }
                } catch (exception: NoSuchElementException) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@post
                }

                val repositoryRecipeAddRequest = ServiceRecipeAddRequestMapper.from(
                    request,
                    authorId,
                    { ServiceRecipeStepAddRequestMapper.from(it, stepsWithIds[it]) },
                    recipeImageId
                )

                val recipeToResponse = serviceRecipes.addRecipe(repositoryRecipeAddRequest)

                call.respond(RecipeResponse.ModelMapper.from(recipeToResponse))
            }

            put("/recipes") {
                val (authorId, _) = call.principal<UserPrincipal>()
                    ?: throw UserPrincipalNotAvailable("(put) /recipes")
                val request = call.receive<RecipeUpdateRequest>()

                val recipe = recipesRepository.getRecipeById(request.id)
                if (recipe == null) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@put
                }

                if (recipe.authorId != authorId) {
                    call.respond(HttpStatusCode.Forbidden)
                    return@put
                }

                val recipeImageId: Int?
                val stepsWithIds = mutableMapOf<RecipeUpdateRequest.Step, Int?>()
                try {
                    recipeImageId = tryGetImageId(serviceImages, request.imageUrl)
                    request.steps.forEach { stepsWithIds[it] = tryGetImageId(serviceImages, it.imageUrl) }
                } catch (exception: NoSuchElementException) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@put
                }

                val repositoryRecipeUpdateRequest = ServiceRecipeUpdateRequestMapper.from(
                    request,
                    authorId,
                    { ServiceRecipeStepUpdateRequestMapper.from(it, stepsWithIds[it]) },
                    recipeImageId
                )

                val recipeToResponse = serviceRecipes.updateRecipe(repositoryRecipeUpdateRequest)

                call.respond(RecipeResponse.ModelMapper.from(recipeToResponse))
            }

            delete<RecipeDeleteLocation> { (recipeId) ->
                val (authorId, _) = call.principal<UserPrincipal>()
                     ?: throw UserPrincipalNotAvailable("(put) /recipes")

                val recipe = recipesRepository.getRecipeById(recipeId)
                if (recipe == null) {
                    call.respond(HttpStatusCode.BadRequest)
                    return@delete
                }

                if (recipe.authorId != authorId) {
                    call.respond(HttpStatusCode.Forbidden)
                    return@delete
                }

                val recipeToResponse = serviceRecipes.deleteRecipe(recipeId)
                if (recipeToResponse == null) {
                    call.respond(HttpStatusCode.BadRequest)
                } else {
                    call.respond(RecipeResponse.ModelMapper.from(recipeToResponse))
                }
            }
        }

        get<RecipeSearchLocation> { (q, page, pageSize) ->
            val recipes = serviceRecipes.getRecipe(q, page - 1, pageSize)
            val recipeResponse = recipes.map { RecipeResponse.ModelMapper.from(it) }

            call.respond(PagingResult(recipeResponse, recipeResponse.size))
        }

        get<UserRecipesLocation> { (userId, page, pageSize) ->
            val user = usersRepository.getUserById(userId)

            if (user == null) {
                call.respond(HttpStatusCode.BadRequest)
                return@get
            }

            val recipes = serviceRecipes.getUserRecipes(userId, page - 1, pageSize)
            val recipesToResponse = recipes.map { RecipeResponse.ModelMapper.from(it) }

            call.respond(PagingResult(recipesToResponse, recipesToResponse.size))
        }

        get<RecipeGetLocation> { (id) ->
            val recipe = serviceRecipes.getRecipeById(id)

            if (recipe == null) {
                call.respond(HttpStatusCode.BadRequest)
                return@get
            }

            call.respond(RecipeResponse.ModelMapper.from(recipe))
        }
    }
}

private suspend fun tryGetImageId(
    serviceImages: ServiceImages,
    imageUrl: String?
): Int? {
    var imageId: Int? = null
    if (imageUrl != null) {
        imageId = serviceImages.getImageByUrl(imageUrl)?.id
        if (imageId == null) {
            throw NoSuchElementException()
        }
    }
    return imageId
}
