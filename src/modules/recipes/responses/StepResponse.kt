package modules.recipes.responses

import com.fasterxml.jackson.annotation.JsonProperty

typealias ServiceRecipeStepResponse = core.services.recipes.responses.RecipeResponse.Step

data class StepResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("title") val title: String,
    @JsonProperty("body") val body: String,
    @JsonProperty("recipeId") val recipeId: Int,
    @JsonProperty("number") val number: Int,
    @JsonProperty("imageUrl") val imageUrl: String?
) {
    object ModelMapper {
        fun from(source: ServiceRecipeStepResponse): StepResponse {
            val (id, title, body, recipeId, number, imageUrl) = source

            return StepResponse(id, title, body, recipeId, number, imageUrl)
        }
    }
}
