package modules.recipes.responses

import com.fasterxml.jackson.annotation.JsonProperty

typealias ServiceRecipeResponse = core.services.recipes.responses.RecipeResponse
typealias ServiceRecipeChef = core.services.recipes.responses.RecipeResponse.Chef
typealias ServiceRecipeNationalCuisine = core.services.recipes.responses.RecipeResponse.NationalCuisine
typealias ServiceRecipeAuthorResponse = core.services.recipes.responses.RecipeResponse.Author

data class RecipeResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("title") val title: String,
    @JsonProperty("description") val description: String?,
    @JsonProperty("author") val author: Author,
    @JsonProperty("likesCount") val likesCount: Int,
    @JsonProperty("stepsCount") val stepsCount: Int,
    @JsonProperty("steps") val steps: List<StepResponse>,
    @JsonProperty("imageUrl") val imageUrl: String?,
    @JsonProperty("chef") val chef: Chef?,
    @JsonProperty("nationalCuisine") val nationalCuisine: NationalCuisine?
) {

    data class Author(
        @JsonProperty("id") val id: Int,
        @JsonProperty("name") val name: String
    ) {

        object ModelMapper {
            fun from(source: ServiceRecipeAuthorResponse): Author {
                val (id, name) = source

                return Author(id, name)
            }
        }
    }

    data class Chef(
        @JsonProperty("id") val id: Int?,
        @JsonProperty("fullName") val fullName: String
    ) {

        object ModelMapper {
            fun from(source: ServiceRecipeChef): Chef {
                val (id, fullName) = source

                return Chef(id, fullName)
            }
        }
    }

    data class NationalCuisine(
        @JsonProperty("id") val id: Int?,
        @JsonProperty("name") val name: String
    ) {

        object ModelMapper {
            fun from(source: ServiceRecipeNationalCuisine): NationalCuisine {
                val (id, name) = source

                return NationalCuisine(id, name)
            }
        }
    }

    object ModelMapper {
        fun from(source: ServiceRecipeResponse): RecipeResponse {
            val (id, title, description, author, likesCount, stepsCount, steps, imageUrl, chef, nationalCuisine) = source
            val chefMapped = if (chef != null) { Chef.ModelMapper.from(chef) } else { null }
            val nationalCuisineMapped = if (nationalCuisine != null) {
                NationalCuisine.ModelMapper.from(nationalCuisine)
            } else { null }

            return RecipeResponse(id, title, description, Author.ModelMapper.from(author), likesCount, stepsCount,
                steps.map { StepResponse.ModelMapper.from(it) }, imageUrl, chefMapped, nationalCuisineMapped)
        }
    }
}
