package modules.chefs.responses

import com.fasterxml.jackson.annotation.JsonProperty

typealias ChefsRepositorySearchResponse = core.repositories.chefs.responses.ChefSearchResponse

data class ChefResponse(
    @JsonProperty("id") val id: Int,
    @JsonProperty("fullName") val fullName: String
) {

    object ModuleMapper {
        fun from(source: ChefsRepositorySearchResponse): ChefResponse {
            val (id, fullName) = source

            return ChefResponse(id, fullName)
        }
    }
}