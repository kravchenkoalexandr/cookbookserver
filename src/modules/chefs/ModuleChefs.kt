package modules.chefs

import core.repositories.chefs.ChefsRepositoryImpl
import core.repositories.chefs.responses.ChefSearchResponse
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import modules.chefs.responses.ChefResponse

fun Application.moduleChefs() {
    val chefsRepository = ChefsRepositoryImpl()

    routing {
        get("/chefs") {
            call.respond(chefsRepository.getAll().map { ChefResponse.ModuleMapper.from(it) })
        }
    }
}