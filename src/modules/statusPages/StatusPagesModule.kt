package modules.statusPages

import com.fasterxml.jackson.module.kotlin.MissingKotlinParameterException
import core.repositories.users.UserAlreadyExistsException
import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.StatusPages
import io.ktor.http.HttpStatusCode
import io.ktor.response.respond

fun Application.moduleStatusPages() {
    install(StatusPages) {
        // TODO: Log exceptions
        exception<Throwable> {
            call.respond(HttpStatusCode.InternalServerError)
        }

        exception<MissingKotlinParameterException> {
            call.respond(HttpStatusCode.BadRequest)
        }

        exception<UserAlreadyExistsException> { cause ->
            call.respond(HttpStatusCode.Unauthorized, cause.localizedMessage)
        }
    }
}
