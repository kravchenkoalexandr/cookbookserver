package app

import com.typesafe.config.ConfigFactory
import io.ktor.config.HoconApplicationConfig
import io.ktor.util.KtorExperimentalAPI

object AppConfig {
    @KtorExperimentalAPI
    private val appConfig = HoconApplicationConfig(ConfigFactory.load())

    @KtorExperimentalAPI
    val address = "http://127.0.0.1:8080"
    //val address = "http://" + appConfig.property("ktor.deployment.host").getString() + ":" +
    //    appConfig.property("ktor.deployment.port").getString()

    object Static {
        object Images {
            @KtorExperimentalAPI
            val dir = appConfig.property("static.images.dir").getString()
            @KtorExperimentalAPI
            val resDir = appConfig.property("static.images.resDir").getString()
            @KtorExperimentalAPI
            val route = appConfig.property("static.images.route").getString()
        }
    }

    object Database {
        @KtorExperimentalAPI
        val url = appConfig.property("db.jdbcUrl").getString()
        @KtorExperimentalAPI
        val user = appConfig.property("db.dbUser").getString()
        @KtorExperimentalAPI
        val password = appConfig.property("db.dbPassword").getString()
    }

    object Jwt {
        @KtorExperimentalAPI
        val secret = appConfig.property("jwt.secret").getString()
        @KtorExperimentalAPI
        val realm = appConfig.property("jwt.realm").getString()
        @KtorExperimentalAPI
        val audience = appConfig.property("jwt.audience").getString()
        @KtorExperimentalAPI
        val issuer = appConfig.property("jwt.issuer").getString()
    }
}
