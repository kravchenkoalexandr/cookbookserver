package com.me.db

import app.AppConfig
import com.zaxxer.hikari.HikariConfig
import com.zaxxer.hikari.HikariDataSource
import io.ktor.util.KtorExperimentalAPI
import org.flywaydb.core.Flyway
import org.jetbrains.exposed.sql.Database

object DatabaseFactory {
    private val dbConfig = AppConfig.Database

    @KtorExperimentalAPI
    fun init() {
        Database.connect(hikari())
        Flyway.configure()
            .dataSource(
                dbConfig.url,
                dbConfig.user,
                dbConfig.password
            )
            .load()
            .migrate()
    }

    @KtorExperimentalAPI
    private fun hikari(): HikariDataSource {
        val config = HikariConfig().apply {
            driverClassName = "org.postgresql.Driver"
            jdbcUrl = dbConfig.url
            username = dbConfig.user
            password = dbConfig.password
            maximumPoolSize = 3
            isAutoCommit = false
            transactionIsolation = "TRANSACTION_REPEATABLE_READ"
        }
        config.validate()
        return HikariDataSource(config)
    }
}
