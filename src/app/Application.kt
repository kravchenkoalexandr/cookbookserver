package app

import com.fasterxml.jackson.databind.SerializationFeature
import com.me.db.DatabaseFactory
import io.ktor.application.Application
import io.ktor.application.install
import io.ktor.auth.Principal
import io.ktor.features.ContentNegotiation
import io.ktor.jackson.jackson
import io.ktor.locations.KtorExperimentalLocationsAPI
import io.ktor.locations.Locations
import io.ktor.util.KtorExperimentalAPI
import java.lang.Exception
import modules.auth.moduleAuth
import modules.chefs.moduleChefs
import modules.files.moduleFiles
import modules.nationalCuisines.moduleNationalCuisines
import modules.recipes.moduleRecipes
import modules.statusPages.moduleStatusPages

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

data class UserPrincipal(val id: Int, val name: String) : Principal

class UserPrincipalNotAvailable(inRoute: String) : Exception("User principal not available in route '$inRoute'")

@KtorExperimentalAPI
@KtorExperimentalLocationsAPI
fun Application.moduleMain() {
    DatabaseFactory.init()
    install(Locations)
    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }
    moduleStatusPages()
    moduleAuth()
    moduleFiles()
    moduleRecipes()
    moduleChefs()
    moduleNationalCuisines()
}
