ALTER TABLE RECIPES
    DROP CONSTRAINT recipes_image_id_fkey,
    ADD CONSTRAINT recipes_image_id_fkey FOREIGN KEY (image_id) REFERENCES IMAGES ON DELETE SET NULL,

    DROP CONSTRAINT recipes_author_id_fkey,
    ADD CONSTRAINT recipes_author_id_fkey FOREIGN KEY (author_id) REFERENCES USERS ON DELETE CASCADE;

ALTER TABLE RECIPE_STEPS
    DROP CONSTRAINT recipe_steps_image_id_fkey,
    ADD CONSTRAINT recipe_steps_image_id_fkey FOREIGN KEY (image_id) REFERENCES IMAGES ON DELETE SET NULL,

    DROP CONSTRAINT recipe_steps_recipe_id_fkey,
    ADD CONSTRAINT recipe_steps_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES RECIPES ON DELETE CASCADE;

ALTER TABLE LIKES
    DROP CONSTRAINT likes_owner_id_fkey,
    ADD CONSTRAINT likes_owner_id_fkey FOREIGN KEY (owner_id) REFERENCES USERS ON DELETE CASCADE,

    DROP CONSTRAINT likes_recipe_id_fkey,
    ADD CONSTRAINT likes_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES RECIPES ON DELETE CASCADE;
