-- CHEF TABLE
CREATE TABLE CHEFS (
    -- chef id
    id      SERIAL PRIMARY KEY,
    -- chef name
    full_name    VARCHAR(256) CHECK (full_name <> '') UNIQUE NOT NULL
);

-- NATIONAL CUISINES
CREATE TABLE NATIONAL_CUISINES (
    -- national cuisine id
    id      SERIAL PRIMARY KEY,
    -- national cuisine name
    name    VARCHAR(256) CHECK (name <> '') UNIQUE NOT NULL
);

ALTER TABLE RECIPES
    ADD COLUMN chef_id INTEGER REFERENCES CHEFS ON DELETE SET NULL NULL ,
    ADD COLUMN national_cuisines_id INTEGER REFERENCES NATIONAL_CUISINES ON DELETE SET NULL NULL;