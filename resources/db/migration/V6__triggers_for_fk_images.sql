ALTER TABLE IMAGES ADD COLUMN ref_count INTEGER DEFAULT 0 NOT NULL;

DROP FUNCTION IF EXISTS process_update_steps() CASCADE ;
DROP TRIGGER IF EXISTS update_steps ON RECIPE_STEPS;

/** STEPS TRIGGER **/

CREATE FUNCTION process_update_steps() RETURNS TRIGGER AS $recipe_step$
BEGIN

    IF (TG_OP = 'DELETE') THEN
        /** Update steps_count column in table RECIPES **/
        UPDATE RECIPES SET steps_count = steps_count - 1 WHERE id = OLD.recipe_id;
        /** Update ref_count column in table IMAGES **/
        IF (OLD.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
        END IF;
        /** Return old value **/
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        IF (OLD.recipe_id != NEW.recipe_id) THEN
            RAISE EXCEPTION 'Запрещено изменение recipe_id';
        END IF;
        /** Update ref_count column in table IMAGES **/
        IF (OLD.image_id IS NULL AND NEW.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        ELSIF (OLD.image_id IS NOT NULL AND NEW.image_id IS NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
        ELSIF (OLD.image_id IS NOT NULL AND NEW.image_id IS NOT NULL AND OLD.image_id <> NEW.image_id) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        END IF;
        /** Return new value **/
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        /** Update steps_count column in table RECIPES **/
        UPDATE RECIPES SET steps_count = steps_count + 1 WHERE id = NEW.recipe_id;
        /** Update ref_count column in table IMAGES **/
        IF (NEW.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        END IF;
        /** Return new value **/
        RETURN NEW;
    END IF;
    RETURN NULL;

END;
$recipe_step$ LANGUAGE plpgsql;

CREATE TRIGGER update_steps BEFORE INSERT OR UPDATE OR DELETE ON RECIPE_STEPS
    FOR EACH ROW EXECUTE PROCEDURE process_update_steps();

/** RECIPES TRIGGER **/

CREATE FUNCTION process_update_recipes() RETURNS TRIGGER AS $recipe$
BEGIN

    IF (TG_OP = 'DELETE') THEN
        /** Update ref_count column in table IMAGES **/
        IF (OLD.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
        END IF;
        /** Return old value **/
        RETURN OLD;
    ELSIF (TG_OP = 'UPDATE') THEN
        IF (OLD.author_id != NEW.author_id) THEN
            RAISE EXCEPTION 'Запрещено изменение author_id';
        END IF;
        /** Update ref_count column in table IMAGES **/
        IF (OLD.image_id IS NULL AND NEW.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        ELSIF (OLD.image_id IS NOT NULL AND NEW.image_id IS NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
        ELSIF (OLD.image_id IS NOT NULL AND NEW.image_id IS NOT NULL AND OLD.image_id <> NEW.image_id) THEN
            UPDATE IMAGES SET ref_count = ref_count - 1 WHERE id = OLD.image_id;
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        END IF;
        /** Return new value **/
        RETURN NEW;
    ELSIF (TG_OP = 'INSERT') THEN
        /** Update ref_count column in table IMAGES **/
        IF (NEW.image_id IS NOT NULL) THEN
            UPDATE IMAGES SET ref_count = ref_count + 1 WHERE id = NEW.image_id;
        END IF;
        /** Return new value **/
        RETURN NEW;
    END IF;
    RETURN NULL;

END;
$recipe$ LANGUAGE plpgsql;

CREATE TRIGGER update_recipes BEFORE INSERT OR UPDATE OR DELETE ON RECIPES
    FOR EACH ROW EXECUTE PROCEDURE process_update_recipes();