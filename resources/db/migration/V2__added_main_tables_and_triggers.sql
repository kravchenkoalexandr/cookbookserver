ALTER TABLE USERS ADD COLUMN name VARCHAR(32) CHECK (name = '' IS NOT FALSE) NULL;

ALTER TABLE USERS DROP COLUMN email;

ALTER TABLE USERS ADD COLUMN login VARCHAR(32) CHECK (login <> '') UNIQUE NOT NULL;

ALTER TABLE USERS ALTER COLUMN password TYPE VARCHAR(100);
ALTER TABLE USERS ADD CONSTRAINT check_not_empty CHECK (password <> '');
ALTER TABLE USERS ALTER COLUMN password SET NOT NULL;

ALTER TABLE USERS DROP COLUMN active;

ALTER TABLE USERS ADD COLUMN access_token VARCHAR(100) CHECK (access_token <> '') UNIQUE NOT NULL;

/**
CREATE TABLE USERS (
    -- user id
    id              SERIAL PRIMARY KEY,
    -- user name (optional)
    name            VARCHAR(32) CHECK (name = '' IS NOT FALSE) NULL,
    -- user login
    login           VARCHAR(32) CHECK (login <> '') UNIQUE NOT NULL,
    -- user password (must be encrypted)
    password        VARCHAR(32) CHECK (password <> '') NOT NULL,
    -- access token for user
    access_token    VARCHAR(100) CHECK (access_token <> '') UNIQUE NOT NULL
);
**/

-- IMAGES TABLE
CREATE TABLE IMAGES (
    -- image id
    id      SERIAL PRIMARY KEY,
    -- path to image
    path    VARCHAR(256) CHECK (path <> '') NOT NULL,
    -- image name
    name    VARCHAR(256) CHECK (name <> '') NOT NULL,
    -- path/image must be uniques for each image
    UNIQUE (path, name)
);

-- RECIPE TABLE
CREATE TABLE RECIPES (
    -- recipe id
    id          SERIAL PRIMARY KEY,
    -- recipe title
    title       VARCHAR(100) CHECK (title <> '') NOT NULL,
    -- recipe's author id
    author_id   INTEGER REFERENCES USERS NOT NULL,
    -- recipe steps count
    steps_count INTEGER DEFAULT 0 NOT NULL,
    -- recipe likes count
    likes_count INTEGER DEFAULT 0 NOT NULL,
    -- recipe image (optional)
    image_id    INTEGER REFERENCES IMAGES NULL
);

-- RECIPE STEP (one of the steps of the recipe)
CREATE TABLE RECIPE_STEPS (
    -- step id
    id          SERIAL PRIMARY KEY,
    -- FK to RECIPE table
    recipe_id   INTEGER REFERENCES RECIPES NOT NULL,
    -- step number
    number      INTEGER CHECK (number >= 0) NOT NULL,
    -- step title
    title       VARCHAR(100) CHECK (title <> '') NOT NULL,
    -- step body
    body        VARCHAR(300) CHECK (body <> '') NOT NULL,
    -- step image (optional)
    image_id    INTEGER REFERENCES IMAGES NULL,
    -- step number must be unique for recipe with id recipe_id
    UNIQUE (recipe_id, number)
);

-- LIKES TABLE
CREATE TABLE LIKES (
    -- like id
    id          SERIAL PRIMARY KEY,
    -- like's owner
    owner_id    INTEGER REFERENCES USERS NOT NULL,
    -- liked recipe
    recipe_id   INTEGER REFERENCES RECIPES NOT NULL
);

CREATE FUNCTION process_update_likes() RETURNS TRIGGER AS $like$
    BEGIN

        IF (TG_OP = 'DELETE') THEN
            UPDATE RECIPE
                SET likes_count = likes_count - 1
                WHERE id = OLD.recipe_id;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            IF (OLD.recipe_id != NEW.recipe_id OR OLD.owner_id != NEW.owner_id) THEN
                RAISE EXCEPTION 'Запрещено изменение recipe_id или owner_id';
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            UPDATE RECIPE
                SET likes_count = likes_count + 1
                WHERE id = NEW.recipe_id;
            RETURN NEW;
        END IF;
        RETURN NULL;

    END;
$like$ LANGUAGE plpgsql;

CREATE TRIGGER update_likes BEFORE INSERT OR UPDATE OR DELETE ON LIKES
    FOR EACH ROW EXECUTE PROCEDURE process_update_likes();

CREATE FUNCTION process_update_steps() RETURNS TRIGGER AS $recipe_step$
    BEGIN

        IF (TG_OP = 'DELETE') THEN
            UPDATE RECIPES
                SET steps_count = steps_count - 1
                WHERE id = OLD.recipe_id;
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            IF (OLD.recipe_id != NEW.recipe_id) THEN
                RAISE EXCEPTION 'Запрещено изменение recipe_id';
            END IF;
            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            UPDATE RECIPES
                SET steps_count = steps_count + 1
                WHERE id = NEW.recipe_id;
            RETURN NEW;
        END IF;
        RETURN NULL;

    END;
$recipe_step$ LANGUAGE plpgsql;

CREATE TRIGGER update_steps BEFORE INSERT OR UPDATE OR DELETE ON RECIPE_STEPS
    FOR EACH ROW EXECUTE PROCEDURE process_update_steps();